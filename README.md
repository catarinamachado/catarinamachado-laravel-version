## Hi!

This is the repository with a Laravel version of my personal website.
[Here](https://catarinamachado.github.io) you can see the website's appearance (using only HTML and CSS), which is exactly the same as Laravel version.

## Contributing

The design (HTML/CSS) of this website was made by
[HTML5 UP](https://html5up.net/). Many thanks!
